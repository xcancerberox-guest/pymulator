from pymulator import Simulation
from pymulator.models import ModeloIntegrador, ConstantGenerator


config_integrator = {
    "gen": {
        "model": ConstantGenerator,
        "initial_values": {
            "amplitude": 2
            },
        "inputs": {},
        },
    "integrador": {
        "model": ModeloIntegrador,
        "initial_values": {
            "initial_state": 0,
            "gain": 1
            },
        "inputs": {"input_signal": ("gen", "output_signal")},
        },
    }


if __name__ == "__main__":
    simulacion = Simulation(config_integrator, 1)
    simulacion.loop(10)
    print(simulacion.data)
