from pymulator import Simulation
from pymulator.models import ModeloIntegrador, SquareGenerator


config_gen = {
    "generator": {
        "model": SquareGenerator,
        "initial_values": {
            "freq": 0.05,
            "duty": 0.5,
            "amplitude": 2
            },
        "inputs": {},
        }
    }

if __name__ == "__main__":
    simulacion = Simulation(config_gen, 1)
    simulacion.loop(100)
    print(simulacion.data)
