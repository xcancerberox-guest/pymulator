from pymulator import Simulation
from pymulator.models import ModeloIntegrador, SquareGenerator

config_cascade = {
    "generator": {
        "model": SquareGenerator,
        "initial_values": {
            "freq": 0.05,
            "duty": 0.5,
            "amplitude": 2
            },
        "inputs": {},
        },
    "integrador": {
        "model": ModeloIntegrador,
        "initial_values": {
            "initial_state": 0,
            "gain": 1
            },
        "inputs": {"input_signal": ("generator", "output_signal")},
        },
    }


if __name__ == "__main__":
    simulacion = Simulation(config_cascade, 1)
    simulacion.loop(20)
    print(simulacion.data)
