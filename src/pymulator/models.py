import numpy as np
from scipy.integrate import odeint
from scipy.signal import square as square_signal


class ModeloIntegrador():
    """
    input_signal ----| gain |----| \int |--- output_signal
    """
    def __init__(self, initial_state=0, gain=0):
        self.state = initial_state
        self.gain = gain

    def state_equation(self, state, time, input_signal):
        return [self.gain*input_signal]

    def output(self):
        return {"integral": self.state}

    def current_state(self):
        return [self.state]

    def next_state(self, delta_t, input_signal):
        solution = odeint(self.state_equation, self.current_state(),
                          [0, delta_t], args=(input_signal,))
        self.state = solution[1, 0]


class SquareGenerator():
    """
    | gen |--- output_signal
    """
    def __init__(self, freq, duty, amplitude):
        self.freq = freq
        self.amplitude = amplitude
        self.duty = duty
        self.time = 0

        self.output_signal = square_signal(0, self.duty)*self.amplitude

    def output(self):
        return {"output_signal": self.output_signal}

    def next_state(self, delta_t):
        self.time += delta_t
        self.output_signal = square_signal(2*np.pi*self.freq*self.time,
                                           self.duty)*self.amplitude


class StepGenerator():
    """
    | gen |--- output_signal
    """
    def __init__(self, amplitude, delay):
        self.amplitude = amplitude
        self.delay = delay
        self.time = 0
        self.output_signal = 0

    def output(self):
        return {"output_signal": self.output_signal}

    def next_state(self, delta_t):
        self.time += delta_t
        if (self.time >= self.delay):
            self.output_signal = self.amplitude
        else:
            self.output_signal = 0


class ConstantGenerator():
    """
    | gen |--- output_signal
    """
    def __init__(self, amplitude):
        self.amplitude = amplitude
        self.time = 0
        self.output_signal = amplitude

    def output(self):
        return {"output_signal": self.output_signal}

    def next_state(self, delta_t):
        self.time += delta_t
