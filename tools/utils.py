def to_milimeters(position):
    return position/1e-3


def run_from_ipython():
    try:
        __IPYTHON__
        return True
    except NameError:
        return False
